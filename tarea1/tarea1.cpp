#include <iostream>
#include <fstream>
#include <math.h>
#include <bitset>
#include <vector>
#include <stdlib.h>  

//Definicion de estados:
#define N "00" // Strong Not Taken
#define n "01" // Weakly Not Taken
#define t "10" // Weakly Taken
#define T "11" // Strong Taken

#define P "00" // Strongly Preferd PS
#define p "01" // Weakly Preferd PS
#define g "10" // Weakly Preferd GS
#define G "11" // Strongly Preferd GS

using namespace std;

// Declaracion de funciones
void bimodal(int s, int o, vector<string> outcome, vector<string> dir, int countBranch);
int gshare(int s, int o, int gh, vector<string> outcome, vector<string> dir, int countBranch, int single);
int getMask(int bits);
int pshare(int s, int o, int ph, vector<string> outcome, vector<string> dir, int countBranch, int single);
static inline long getlastbits(long num, int nbbits);
void torneo(int s, int o, int gh, int ph, vector<string> outcome, vector<string> dir, int countBranch);

int main (int argc, char** argv) {
    //leer las entradas de la linea de comandos
    string a;
    vector<string> outcome;
    vector<string> dir;
    int countBranch = 0;
    while (cin){   
        cin >> a;
        if (a == "T" | a == "N"){outcome.push_back(a);countBranch ++;} // divir por outcome
        else{dir.push_back(a); } //dividr por direccion
    }
    countBranch --;
    outcome.pop_back();

    //convertir argumentos de entrada en int.
    long s,bp,gh,ph,o;
    s = strtol(argv[2], NULL, 10);
    bp = strtol(argv[4], NULL, 10);
    gh = strtol(argv[6], NULL, 10);
    ph = strtol(argv[8], NULL, 10);
    o = strtol(argv[10], NULL, 10);
    //tamano de la tabla 
    int z = pow(2,s);

    //impresion de los parametro de prediccion dados
    cout << "Prediction parameters:" << endl << "Branch prediction type: ";
    if (bp == 0){cout << "Bimodal" << endl;}
    if (bp == 1){cout << "GlobalShare" << endl;}
    if (bp == 2){cout << "PrivateShare" << endl;}
    if (bp == 3){cout << "Torneo" << endl;}
    cout << "PBHT size (entries): " << z << endl << "Global history register size: " << gh << endl << "Private history register size: " << ph << endl;

    //llamado a las funciones
    if (bp==0){bimodal(s, o, outcome, dir, countBranch);}
    if (bp==1){gshare(s, o, gh, outcome, dir, countBranch, 0);}
    if (bp==2){pshare(s, o, ph, outcome, dir, countBranch, 0); }
    if (bp==3){torneo(s, o, gh, ph, outcome, dir, countBranch); }
}

/* El predictor bimodal utiliza un arreglo de contadores de 2 bits. El tamano del alegro lo establece el parametro s,
El numero de entradas es 2**s. Se busca selecciona un contador de la table utilizando los ultimos bits del contador del programa.
Las entradas que recive son s para las dimensiones de la tabla, o para ver si se escribe el resultado en un archivo,
el vector de outcome con los saltos reales, el vector de direcciones y el contador de branch
*/
void bimodal(int s, int o, vector<string> outcome, vector<string> dir, int countBranch) {
    // Inicializando contadores para cantidad de predicciones correcta o incorrectas
    int countCT = 0;
    int countIT = 0;
    int countCN = 0;
    int countIN = 0;
    int prcnt = 0;
    int z = pow(2,s);
    //Se crea la table de tamaño 2**s
    string tbl[z];

    // arcvhio en caso de que la opcion o este habilitada
    ofstream out;
    if (o == 1){
            string out_file_name = "Bimodal.txt";
            out.open(out_file_name.c_str());
            //encabezado del archivo
            out << "State | "<< "Prediction | " << "Outcome | " << "Correct?" << endl;
        }

    // inicializando contadores en Strong Not taken
    for(int i=0;i<z;i++){
        tbl[i] = N;
    }  

    string taken;
    string direc;
    long pc;
    string correct;
    string pred;
    string::size_type sz;

    //Por cada linea de entrada se repite lo siguiente:
    for (int d = 0; d < outcome.size(); ++d){
        // datos de direccion de salto
        taken = outcome[d];
        direc = dir[d];
        //convertir a int la direccion
        pc = stol(direc,&sz);
        // indice de la tabla
        int index = pc % z;
        int tval;
        //convertir los estados a valores numericos
        if (tbl[index] == N){tval = 0;}
        if (tbl[index] == n){tval = 1;}
        if (tbl[index] == t){tval = 2;}
        if (tbl[index] == T){tval = 3;}
        
        bool prediction;
        //casos: se lleva el conteo de aciertos y fallos y se modifica la entrada de la tabla dependiendo de si era correcto o no
        if(taken == "NT") {
            if(tval == 0){
                prediction = 0;
                countCN ++;
            } 
            else if(tval == 1){
                prediction = 0;
                tbl[index] = N;
                countCN ++;
            } 
            else if(tval == 2){
                prediction = 1;
                tbl[index] = n;
                countIN ++;
            } 
            else if(tval == 3){
                prediction = 1;
                tbl[index] = T;
                countIN ++;
            } 
        } else{
            if(tval == 0){
                prediction = 0;
                tbl[index] = n;
                countIT ++;
            } 
            else if(tval == 1){
                prediction = 0;
                tbl[index] = t;
                countIT ++;
            } 
            else if(tval == 2){
                prediction = 1;
                tbl[index] = T;
                countCT ++;
            } 
            else if(tval == 3){
                prediction = 1;
                countCT ++;
            } 
        }

        // definir string correct para guardar los datos en el archivo
        if (prediction == 0 && taken == "N"){
            correct = " correct";
        }
        else if (prediction == 1 && taken == "T"){
            correct = " correct";
        }
        else {
            correct = " incorrect";
        }

        // En caso de que se quiera un archivo con los resultados
        if (o == 1){
            // convertir boole de prediction en Taken or Not Taken
            if (prediction == 1){
                pred = " T ";
            }
            else{
                pred = " N ";
            }
            // obtener el valor de la tabla en terminos de N n t T
            for(int i=0;i<z;i++){
                    if (tbl[i]==N){out << "N";}
                    if (tbl[i]==n){out << "n";}
                    if (tbl[i]==t){out << "t";}
                    if (tbl[i]==T){out << "T";}
            } 
        //copiar esto al archivo out
        out << pred << taken << correct << endl;
        }
    }
    // calculo de aciertos, fallos y porcentaje 
    prcnt = (countCN+countCT) *100 / (countIN+countIT+countCT+countCN);
    cout << endl << "Simulation results for Bimodal:" << endl;
    cout << "Number of branch: " << countBranch << endl << "Number of correct prediction of taken branches: " << countCT << endl << "Number of incorrect prediction of taken branches: " << countIT << endl << "Correct prediction of not taken branches: " << countCN << endl << "Incorrect prediction of not taken branches: " << countIN << endl << "Percentage of correct predictions " << prcnt << " %" << endl;
}

/* El GlobalShare utiliza un arreglo de 2**s con contadores de 2 bits y un registro con la historia de las últimos saltos.
La cantidad de estos bits de resgitro está dado por el parametro gh. Se usan los ultimos bits del resultados de una XOR.
Se inicializan todos los contadores en strong not taken. La funcion recibe como entrada los parametro s, o y gh para
el uso mencionado anteeriormente. Ademas recibe el vector outcome y el dir con los saltos y direcciones. 
Por ultimo recibe el contador de branch y un int single que se habilita solo cuando la funcion Torneo lo manda a llamar. 
Esto con el fin de bloquear cierta parte del codigo cuando solo lo este usando en Torneo.
*/
int gshare(int s, int o, int gh, vector<string> outcome, vector<string> dir, int countBranch, int single) {
    // inciando contadores
    int countCT = 0;
    int countIT = 0;
    int countCN = 0;
    int countIN = 0;
    int prcnt = 0;
    int z = pow(2,s);
    //tamano de la tabla 2**s
    string gls[z];
    string bhr;

    // arcvhio en caso de que la opcion o este habilitada
    ofstream out;
    if (o == 1){
            string out_file_name = "GlobalShare.txt";
            out.open(out_file_name.c_str());
            //encabezado del archivo
            out << "State | "<< "Prediction | " << "Outcome | " << "Correct?" << endl;
        }
    // inicializando contadores en Strong Not taken
    for(int i=0;i<z;i++){
        gls[i] = N;
    }  
    
    int history = 0;
    int mask;
    //lamado a la funcion de obtencion de mascara 
    mask = getMask(gh);

    string taken;
    string direc;
    long pc;
    string correct;
    string pred;
    string::size_type sz;
    int contador = 0;    

    //Por cada linea de entrada se repite lo siguiente:
    for (int d = 0; d < outcome.size(); ++d){
        // datos de direccion de salto
        taken = outcome[d];
        direc = dir[d];
        //convertir a int la direccion
        pc = stol(direc,&sz)%s;
        // indice de la tabla adaptado para el tamno de
        int index = (pc) ^ (history & mask);
        int tval;        
        //convertir los estados a valores numericos
        if (gls[index] == N){tval = 0;}
        if (gls[index] == n){tval = 1;}
        if (gls[index] == t){tval = 2;}
        if (gls[index] == T){tval = 3;}
        
        bool prediction;
        //casos: se lleva el conteo de aciertos y fallos y se modifica la entrada de la tabla dependiendo de si era correcto o no
        if(taken == "N") {
            if(tval == 0){
                prediction = 0;
                countCN ++;
            }
            else if(tval == 1){
                prediction = 0;
                countCN ++;
                gls[index] = N;
            }
            else if(tval == 2){
                prediction = 1;
                countIN ++;
                gls[index] = n;
            }
            else{
                prediction = 1;
                countIN ++;
                gls[index] = t;
            }
            history <<= 1; // se modifica el registro de historia
        } else{
            if(tval == 3){
                prediction = 1;
                countCT ++;
            }
            else if(tval == 2){
                prediction = 1;
                countCT ++;
                gls[index] = T;
            }
            else if(tval == 1){
                prediction = 0;
                countIT ++;
                gls[index] = t;
            }
            else{
                prediction = 0;
                countIT ++;
                gls[index] = n;
            }
            history <<= 1;
            history |= 1;
        }

        // definir string correct para guardar los datos en el archivo
        if (prediction == 0 && taken == "N"){
            correct = " correct";
        }
        else if (prediction == 1 && taken == "T"){
            correct = " correct";
        }
        else {
            correct = " incorrect";
        }

        // single estara habilitado solo cuando Torneo la mande a llamar
        if (single != 0 && single == contador){
            return prediction; //devuelve el valor de prediccion que busca la funcion Torneo en el momento de llamarla
        }

        // En caso de que se quiera un archivo con los resultados
        if (o == 1){
            // convertir boole de prediction en Taken or Not Taken
            if (prediction == 1){
                pred = " T ";
            }
            else{
                pred = " N ";
            }  
            // obtener el valor de la tabla en terminos de N n t T
            for(int i=0;i<z;i++){
                    if (gls[i]==N){out << "N";}
                    if (gls[i]==n){out << "n";}
                    if (gls[i]==t){out << "t";}
                    if (gls[i]==T){out << "T";}
            }
            //copiar esto al archivo out
            out << pred << taken << correct << endl;
        } 
        contador ++;
    }

    // si no ha sido invocada por Torneo, que imprima sus resultados de simulacion
    if (single == 0)
    {
        // calculo de aciertos, fallos y porcentaje 
        prcnt = (countCN+countCT) *100 / (countIN+countIT+countCT+countCN);
        cout << endl << "Simulation results for GlobalShare:" << endl;
        cout << "Number of branch: " << countBranch << endl << "Number of correct prediction of taken branches: " << countCT << endl << "Number of incorrect prediction of taken branches: " << countIT << endl << "Correct prediction of not taken branches: " << countCN << endl << "Incorrect prediction of not taken branches: " << countIN << endl << "Percentage of correct predictions " << prcnt << " %" << endl;
        return 0;
    }
}

// mascara dependiendo de los bits gh
int getMask(int bits) {
    int result = 0;
    switch(bits) {
        case 3:
            result = 0b00000000111;
            break;
        default:
            result = 0b00000000011;
            break;
    }
    return result;
}

/* El PrivateShare tiene toda una tabla con la historia de cada salto.
Este tamano y el de la tabla vienen dados por s. La Cantidad de bits de historia que se almacanen en cada entrada esta dada por ph.
Para hacer el indice se utilizan los ultimos bits del resultado de la XOR del contador y el registro privado de historia.
Todas los contadores inician en Strong Not Taken. Esta funcion recibe los mismo parametros que la anterior, con la diferencia de que recibe 
el ph mencionado anteriormente en lugar del gh.
*/
int pshare(int s, int o, int ph, vector<string> outcome, vector<string> dir, int countBranch, int single) {
    // inciando contadores
    int countCT = 0;
    int countIT = 0;
    int countCN = 0;
    int countIN = 0;
    int prcnt = 0;
    int z = pow(2,s);
    //tamano de la tabla 2**s
    string pht[z][ph];
    string bhr;

    // arcvhio en caso de que la opcion o este habilitada
    ofstream out;
    if (o == 1){
            string out_file_name = "PrivateShare.txt";
            out.open(out_file_name.c_str());
            out << "State | "<< "Prediction | " << "Outcome | " << "Correct?" << endl;
        }

    // inicializando contadores en Strong Not taken y tabla de registros en 1
    for(int i=0;i<ph;i++){
        bhr += '1';
    }
    for(int i=0;i<z;i++){
        for(int j=0;j<ph;j++){
            pht[i][j] = N;
        }
    }
    
    string taken;
    string direc;
    long pc;
    string correct;
    string pred;
    string::size_type sz;
    int contador = 0;

    //Por cada linea de entrada se repite lo siguiente:
    for (int d = 0; d < outcome.size(); ++d){
        // datos de direccion de salto
        taken = outcome[d];
        direc = dir[d];
        //convertir a int la direccion
        pc = stol(direc,&sz)%s;

        //obtener los ultimos bits por medio de la funcion getlastbits
        int val = getlastbits(pc, s); 
        // convertir bjrval a un valor unsigned long long int 
        int bhrval = strtoull(bhr.c_str(),NULL,2);
        
        bool prediction;
        
        bhrval = strtoull(bhr.c_str(),NULL,2);
        // actualizando la tabla de registros pht
        //casos: se lleva el conteo de aciertos y fallos y se modifica la entrada de la tabla dependiendo de si era correcto o no
        if(taken == "T"){
            if(pht[val][bhrval]==T||pht[val][bhrval]==t){
                pht[val][bhrval]=T;
                countCT ++;
                prediction=true;
            }
            else if(pht[val][bhrval]==n){
                pht[val][bhrval]=T;
                countIT ++;
                prediction=false;
            }
            else if(pht[val][bhrval]==N){
                pht[val][bhrval]=n;
                countIT ++;
                prediction=false;
            }
        }
        else {
            if(pht[val][bhrval]==t){
                pht[val][bhrval]=N;
                countIN ++;
                prediction=true;
            }
            else if(pht[val][bhrval]==N||pht[val][bhrval]==n){
                pht[val][bhrval]=N;
                countCN ++;
                prediction=false;
            }
            else if(pht[val][bhrval]==T){
                pht[val][bhrval]=t;
                countIN ++;
                prediction=true;
            }
        }
        
        // actualizar el registro de historia de salto        
        for(int i=ph-1;i>0;i--){              
            bhr[i]=bhr[i-1];
        }
        if(ph>1){
            if(taken == "T"){
                    bhr[0] = '1'; 
                }
                else {
                    bhr[0] = '0';
                } 
        }

        // definir string correct para guardar los datos en el archivo
        if (prediction == 0 && taken == "N"){
            correct = " correct";
        }
        else if (prediction == 1 && taken == "T"){
            correct = " correct";
        } 
        else {
            correct = " incorrect";
        }

        // single estara habilitado solo cuando Torneo la mande a llamar
        if (single != 0 && single == contador){
            return prediction;
        }

        // En caso de que se quiera un archivo con los resultados
        if (o == 1){
            // convertir boole de prediction en Taken or Not Taken
            if (prediction == 1){
                pred = " T ";
            }
            else{
                pred = " N ";
            }
            // obtener el valor de la tabla en terminos de N n t T
            for(int i=0;i<z/ph;i++){
                for(int j=0;j<ph;j++){
                    if (pht[i][j]==N){out << "N";}
                    if (pht[i][j]==n){out << "n";}
                    if (pht[i][j]==t){out << "t";}
                    if (pht[i][j]==T){out << "T";}
                }
            }  
            
            //copiar esto al archivo out
            out << pred << taken << correct << endl;
        }
        contador++;
    }

    // si no ha sido invocada por Torneo, que imprima sus resultados de simulacion
    if (single == 0){
        // calculo de aciertos, fallos y porcentaje 
        prcnt = (countCN+countCT) *100 / (countIN+countIT+countCT+countCN);
        cout << endl << "Simulation results for PrivateShare:" << endl;
        cout << "Number of branch: " << countBranch << endl << "Number of correct prediction of taken branches: " << countCT << endl << "Number of incorrect prediction of taken branches: " << countIT << endl << "Correct prediction of not taken branches: " << countCN << endl << "Incorrect prediction of not taken branches: " << countIN << endl << "Percentage of correct predictions " << prcnt << " %" << endl;
        return 0;
    }
}

//funcion para obtener los ultimos nbbits del numero dado 
static inline long getlastbits(long num, int nbbits) {
   return num & ((1L<<nbbits)-1);
} 

/* Se utilizan los predictores GlobalShare y PrivateShare en paralelo. Cada uno precide de forma independiente y un selector escoge entre ellos.
Este selector hace la funcion de metapredictor con tamano 2**s y contadores de 2 bits. Se indica el predictor a usar. Inicialmente están en strong not taken.
El metapredictor se inicializa en el estado strongly prefer pshare. Las entradas son las mismas que las de las funciones Gshare y Pshare ya que éste se encarga de llamarla.
*/
void torneo(int s, int o, int gh, int ph, vector<string> outcome, vector<string> dir, int countBranch) {
    // inciando contadores
    int countCT = 0;
    int countIT = 0;
    int countCN = 0;
    int countIN = 0;
    int prcnt = 0;
    int z = pow(2,s);
    //selector
    string selectorTbl[z];

    // inicializarlo en storngly prefer PS
    for(int i = 0; i < z; i++) {
        selectorTbl[i] = P;
    }

    // arcvhio en caso de que la opcion o este habilitada
    ofstream out;
    if (o == 1){
        string out_file_name = "Torneo.txt";
        out.open(out_file_name.c_str());
        //encabezado del archivo
        out << "PC | " << "Predictor | " << "Prediction | " << "Outcome | " << "Correct?" << endl;
    }
    
    string taken;
    string direc;
    long pc;
    string correct;
    string pred;
    string::size_type sz;
    int contador = 0;

    //Por cada linea de entrada se repite lo siguiente:
    for (int d = 0; d < outcome.size(); ++d){
        // datos de direccion de salto
        taken = outcome[d];
        direc = dir[d];
        //convertir a int la direccion
        pc = stol(direc,&sz);
        //indice de la tabla
        int index = pc % z; 
        int sval;
        //convertir los estados a valores numericos
        if (selectorTbl[index] == P){sval = 0;}
        if (selectorTbl[index] == p){sval = 1;}
        if (selectorTbl[index] == g){sval = 2;}
        if (selectorTbl[index] == G){sval = 3;}
        int predPS, predGS;
        string predgs, predps;

        //llamar a las funciones Gshare y Pshare con single = d para obtener el valor de este cicli
        predGS = gshare(s, 0, gh, outcome, dir, countBranch, d);
        predPS = pshare(s, 0, ph, outcome, dir, countBranch, d);
        bool prediction;

        // convertir a string para la comparacion de casos
        if (predGS == 1){predgs = "T";}
        else{ predgs = "N";}
        if (predPS == 1){predps = "T";}
        else{ predps = "N";}

        //casos: se lleva el conteo de aciertos y fallos y se modifica la entrada de la tabla dependiendo de si era correcto o no
        switch(sval) {
            case 0:
                prediction = predPS;
                if(taken != predps){
                    selectorTbl[index] = p;
                    countIT++;
                } 
                else{countCT++;}
                break;
            case 1:
                prediction = predPS;
                if(taken != predps){
                    selectorTbl[index] = g;
                    countIT++;
                } 
                else{
                    selectorTbl[index] = p;
                    countCT++;
                }
                break;
            case 2:
                prediction = predGS;
                if(taken != predgs){
                    selectorTbl[index] = p;
                    countIN++;
                } 
                else{
                    selectorTbl[index] = G;
                    countCN++;
                }
                break;
            case 3:
                prediction = predGS;
                if(taken != predgs){
                    selectorTbl[index] = p;
                    countIN++;
                } 
                else{countCN++;}
                break;
        }
        predPS = 0, predGS = 0;

        // definir string correct para guardar los datos en el archivo
        if (prediction == 0 && taken == "N"){
            correct = " correct";
        }
        else if (prediction == 1 && taken == "T"){
            correct = " correct";
        } 
        else {
            correct = " incorrect";
        }
        // En caso de que se quiera un archivo con los resultados
        if (o == 1){
            // convertir boole de prediction en Taken or Not Taken
            if (prediction == 1){
                pred = " T ";
            }
            else{
                pred = " N ";
            }
            string sel;
            // obtener el valor de la tabla en terminos de N n t T
            if (selectorTbl[index] == P){sel = " P ";}
            if (selectorTbl[index] == p){sel = " P ";}
            if (selectorTbl[index] == g){sel = " G ";}
            if (selectorTbl[index] == G){sel = " G ";}
            //copiar esto al archivo out
            out << direc << sel << pred << taken << correct << endl;
        }
        contador++;
    }
    // calculo de aciertos, fallos y porcentaje 
    prcnt = (countCN+countCT) *100 / (countIN+countIT+countCT+countCN);
    cout << endl << "Simulation results for Torneo:" << endl;
    cout << "Number of branch: " << countBranch << endl << "Number of correct prediction of taken branches: " << countCT << endl << "Number of incorrect prediction of taken branches: " << countIT << endl << "Correct prediction of not taken branches: " << countCN << endl << "Incorrect prediction of not taken branches: " << countIN << endl << "Percentage of correct predictions " << prcnt << " %" << endl;
}
