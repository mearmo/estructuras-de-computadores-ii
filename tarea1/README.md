**Tarea 1: Predictores de salto**
B50545

Compilar:
g++ tarea1.cpp -o tarea1.o -std=c++11

Ejecutar:
gunzip -c branch-trace-gcc.trace.gz|head -200|./tarea1.o -s # -bp # -gh # -ph # -o #

Parametros:

s -- tamaño de la tabla (2**s)

bp -- 0(bimodal) 1(gshare) 2(pshare) 3(torneo)

gh -- tamaño de registro de prediccion global

ph -- tamaño de registro de historia privada

o -- 1(escribir archivo con los resultados)